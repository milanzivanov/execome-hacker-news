import { Component, OnInit } from '@angular/core';
import { HncloneApiService, HnInterface } from '../hnclone-api.service';

@Component({
  selector: 'app-stories',
  templateUrl: './stories.component.html',
  styleUrls: ['./stories.component.scss']
})

export class StoriesComponent implements OnInit {

  stories: HnInterface[];
  showLoader: boolean;

  constructor(private _hackerCloneApiService: HncloneApiService) {
  }

  async ngOnInit() {

    // loader show
    this.showLoader = true;

    this.stories = await this._hackerCloneApiService.fetchStories();

    // loader hide
    this.showLoader = false;


  }
}
