import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HncloneApiService } from './hnclone-api.service';
import { ScrollingModule } from '@angular/cdk/scrolling';

import { AppComponent } from './app.component';
import { StoriesComponent } from './stories/stories.component';
import { ItemComponent } from './item/item.component';
import { CommentListComponent } from './comment-list/comment-list.component';
import { NavComponent } from './nav/nav.component';


@NgModule({
  declarations: [
    AppComponent,
    StoriesComponent,
    ItemComponent,
    CommentListComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ScrollingModule
  ],
  providers: [HncloneApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
