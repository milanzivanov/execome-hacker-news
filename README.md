# ExecomeHackerNews task

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.14

How to run:
  - git clone git@gitlab.com:milanzivanov/execome-hacker-news.git
  - cd execome-hacker-news
  - npm i
  - ng serve --open

## Link preview

[hacker news clone](http://hnclone2019.surge.sh/)


