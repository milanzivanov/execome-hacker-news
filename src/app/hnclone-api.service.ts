import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable()
export class HncloneApiService implements OnInit {
  baseUrl: string;
  
  constructor(private http: HttpClient) {
    this.baseUrl = environment.baseUrl;
  }

  ngOnInit(): void {
  }

  async fetchStories(): Promise<HnInterface[]> {
    const ids = await fetchEx<number[]>(`${this.baseUrl}/topstories.json`);

    const temp = ids.map(p => this.fetchItem(p));
    const result = Promise.all(temp);
    return result;
  }

  fetchItem(id: number): Promise<HnInterface> {
    return fetchEx<HnInterface>(`${this.baseUrl}/item/${id}.json`);
  }

  // comments of comments
  fetchComment<T>(id: T): Promise<HnComments> {
    return fetchEx<HnComments>(`${this.baseUrl}/item/${id}.json`);

  }

  fetchComments(ids: number[]): Promise<HnComments[]> {
    const temp = ids.map( p => this.fetchComment(p));
    const res = Promise.all(temp);
    return res;
  }

}

// new
// This is our utility function, we use generic 
// type to anotate what is the return type from
// the server (we assume that we are getting
// always the json response)
async function fetchEx<T>(address: string) {
  const response = await fetch(address);
  const data = await response.json() as T;
  return data;
}

export interface HnInterface {
  by: string;
  descendants: number;
  id: number;
  kids?: (number)[] | null;
  score: number;
  time: number;
  title: string;
  type: string;
  url: string;
}

export interface HnComments {
  by: string;
  id: number;
  kids: number[];
  parent: number;
  text: string;
  time: number;
  type: string;
}
